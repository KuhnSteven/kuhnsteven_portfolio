﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KuhnSteven_CodeProjects
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                Kuhn, Steven
                9/22/2016
                Project & Portfolio I
                Coding Projects
            */

            // Variables
            string userDecision = "";   // string input for the user, if one of the two valid options is typed then decisionBool is true.
            bool validDecisionBool = false;  // allows the loop to run in the beginning

            //  query & intro
            Console.Write("Welcome to my Code Projects & Code Repository! This work was done for my PP1 class, September 2016.\nPlease select one of the following methods to run: SwapName, Backwards, AgeConvert, or TempConvert\nInput: ");
            // SwapName, Backwards, AgeConvert, or TempConvert -- my way of seeing the methods and not have to scroll.
            while (validDecisionBool != true)    // while there is no valid input, do... loop
            {
                userDecision = Console.ReadLine();  // check input very first thing

                if (userDecision == "SwapName")    // specifically checks for SwapName
                {
                    validDecisionBool = true;    // ends the loop
                    SwapName();
                }
                else if (userDecision == "Backwards")   // checks for Backwards
                {
                    validDecisionBool = true;    // ends the loop
                    Backwards();
                }
                else if (userDecision == "AgeConvert")   // checks for AgeConvert
                {
                    validDecisionBool = true;    // ends the loop
                    AgeConvert();
                }
                else if (userDecision == "TempConvert")   // checks for TempConvert
                {
                    validDecisionBool = true;    // ends the loop
                    TempConvert();
                }
                else
                {   // error message
                    Console.WriteLine("ERROR: Invalid input. Please select one of the following methods to run: SwapName, Backwards, AgeConvert, or TempConvert\nInput: ");
                }
            }
        }

        public static void SwapName ()
        {
            // VARIABLES
            string firstName = "";
            string lastName = "";

            Console.WriteLine("Please input your first name."); // query
            firstName = Console.ReadLine();     // input for 1st name
            Console.WriteLine("Please input your last name.");
            lastName = Console.ReadLine();      // input for 2nd name

            Console.WriteLine("So your full name is " + firstName + " " + lastName);        // initial output
            Console.WriteLine("With the power of SwapName, I shall now switch your names!\nYour new name is:" + lastName + " " + firstName);    // secondary/final output
        }
        public static void Backwards()
        {
            // VARIABLES
            string userInput = "";

            Console.WriteLine("Backwards will flip any 6+ word sentence you enter. Give it a try!");    // query
            userInput = Console.ReadLine();

            char[] backwards = userInput.ToCharArray(); // string to array
            Array.Reverse(backwards);   // reversing the array
            Console.WriteLine(backwards);   // output array
        }
        public static void AgeConvert()
        {
            //VARIABLES
            string userName;
            int userAge;

            Console.WriteLine("Welcome user! AgeConvert tells you how old you are in multiple measurements of time.\nBefore we start, please enter your name:");
            userName = Console.ReadLine();
            Console.WriteLine(userName + ", welcome to AgeConvert! Now please enter your age:");    // query
            userAge = int.Parse(Console.ReadLine());
            // all age-related math is thrown into the sentence so it operates right as it's needed.
            Console.WriteLine("Okay " + userName + ", here is your age in a mutltitude of measurements!\nYears: " + userAge + "\nDays: " + userAge * 365 + "\nHours: " + userAge * 21900 + "\nMinutes: " + userAge * 1314000 + "\nSeconds: " + userAge * 78840000);
        }
        public static void TempConvert()
        {
            // VARIABLES
            int userTemp;

            Console.WriteLine("Welcome to TempConvert! Please input a temperature (in Celsius) to be converted:");  // query
            userTemp = int.Parse(Console.ReadLine());
            // math is placed within writeline
            Console.WriteLine("Initial celsius temperature: " + userTemp + "\nConversion to fahrenheit: " + (userTemp * 9 / 5 + 32));

            Console.WriteLine("Now input a temperature (in Fahrenheit) to be converted:");
            userTemp = int.Parse(Console.ReadLine());

            Console.WriteLine("Initial fahrenheit temperature: " + userTemp + "\nConversion to celsius: " + (userTemp - 32 * 9 / 5));
        }
    }
}
