﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KuhnSteven_Lab6
{
    class Program
    {
        static void Main(string[] args)
        {
            // Variables
            string userInput = "";  // for input
            int Cars = 6;
            int final;

            ArrayList modelsList = new ArrayList(); // array 1
            modelsList.Add("C70");
            modelsList.Add("Tacoma");
            modelsList.Add("FRS");
            modelsList.Add("BRZ");
            modelsList.Add("F-Type Coupe");
            modelsList.Add("GTR");

            ArrayList manufsList = new ArrayList(); // array 2
            manufsList.Add("Volvo");
            manufsList.Add("Toyota");
            manufsList.Add("Scion");
            manufsList.Add("Subaru");
            manufsList.Add("Jaguar");
            manufsList.Add("Nissan");

            ArrayList pricesList = new ArrayList(); // array 3
            pricesList.Add("24000");
            pricesList.Add("23300");
            pricesList.Add("25300");
            pricesList.Add("25400");
            pricesList.Add("61000");
            pricesList.Add("101800");

            // Intro / Output
            Console.WriteLine("Ever since I was little, I've loved custom cars. Shows like Pimp My Ride, Fast and Loud, and Top Gear have always been my favorites.");
            Console.WriteLine("Would you like to know about the vehicles I want to get some day? (yes/no)");
            userInput = Console.ReadLine(); // input location

            // Logic
            if (userInput == "yes") // 'yes' input
            {
                final = CarCounter(Cars);
                Console.WriteLine("Cool! For starters, there are " + final + " vehicles I want. I'll start with my first car though.");

                while (modelsList.Count > 0)       // loop that runs while the array has over 0 variables
                {
                    Console.WriteLine("Model: " + modelsList[0] + " - Maker: " + manufsList[0] + " - MSRP: $" + pricesList[0]); // displays each variable                
                    modelsList.RemoveAt(0);
                    manufsList.RemoveAt(0);
                    pricesList.RemoveAt(0);
                    Console.WriteLine("There are " + final + " vehicles left that I want.");   // remaining vars
                }
            }
            else if (userInput == "no") // freedom of choice
            { Console.WriteLine("Alright, talk to you later then."); }
            else
            { Console.WriteLine("Please only input a yes or no answer."); } // in case someone mis-types.

            Console.WriteLine("See you later!");
        }

        static int CarCounter (int Cars)
        {
            Cars = Cars - 1;
            return Cars;
            
        }
    }
}
