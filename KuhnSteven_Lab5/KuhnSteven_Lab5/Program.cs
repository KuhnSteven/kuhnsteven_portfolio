﻿using System;

namespace KuhnSteven_Lab5
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            /* Kuhn, Steven
               1603 SDI
               Lab 5
               Multi-Method Calculator */

            // Variables
            int firstNumber;    // first string w/ int conversion
            int secondNumber;   // second string w/ int conversion
            int result;         // final calculation
            int calculations = 0;   // used to calculate the final answer in each method
            string operation;   // mathematical operation input

            // query & input
            Console.WriteLine("Hello user! please input your first whole number: ");    // query
            firstNumber = Convert.ToInt32(Console.ReadLine());                          // convert to int
            Console.WriteLine("Now pick your second whole number: ");                   // query 2
            secondNumber = Convert.ToInt32(Console.ReadLine());                         // convert 2
            Console.WriteLine("Excellent, your two numbers are " + firstNumber + " and " + secondNumber + "."); // information on selections
            Console.WriteLine("Now please pick one of these four operations: +, -, / or *");    // query 3
            operation = Console.ReadLine(); // string user input for math operation
            Console.WriteLine("You have indicated that you want to use the " + operation + " operation.");  // confirmation on user choice

            // output

            if (operation == "+")       // if the user indicates addition
            {
                result = DisplayAddition(firstNumber, secondNumber, calculations);  // calling variables
                Console.WriteLine("your answer is " + result); }    // output
            else if (operation == "-")  // subtraction
            { result = DisplaySubtraction(firstNumber, secondNumber, calculations); // calling vars
                Console.WriteLine("your answer is " + result);      // output
            }
            else if (operation == "/")  // division
            { if (secondNumber == 0)    // to check if the equation is dividing by 0
                { Console.WriteLine("I can't supply you with a correct answer unfortunately, as you have divided by zero. You should try again!");
                }   // dividing by 0 is not possible, returns an error message as such.
                else
                { result = DisplayDivision(firstNumber, secondNumber, calculations);    // hotline bling the vars
                    Console.WriteLine("your answer is " + result);  // output
                }
            }
            else if (operation == "*")  // multiplication
            { result = DisplayMultiplication(firstNumber, secondNumber, calculations);  // VARS VARS VARS
                Console.WriteLine("your answer is " + result);  // output
            }
       }
        // methods (each is for a seperate math operation)
        static int DisplayAddition(int firstNumber, int secondNumber, int calculations) // method for addition
        {
            calculations = firstNumber + secondNumber;  // there
            return calculations;    // is
        }
        static int DisplaySubtraction(int firstNumber, int secondNumber, int calculations)  // subtraction
        {
            calculations = firstNumber - secondNumber;  // so
            return calculations;    // much
        }
        static int DisplayDivision(int firstNumber, int secondNumber, int calculations) // division. Considered checking for secondNumber == 0 here, forgot, did it further up, too lazy to change what works. =]
        {
            calculations = firstNumber / secondNumber;  // math
            return calculations;    // in
        }
        static int DisplayMultiplication(int firstNumber, int secondNumber, int calculations)   // multiplication
        {
            calculations = firstNumber * secondNumber;  // this
            return calculations;    // code!!
        }
    }
}